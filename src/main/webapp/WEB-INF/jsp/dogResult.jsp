<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dog Result</title>
</head>
<body>
Your dog is named ${ greeting.name }.  He is a ${ greeting.age }-year-old ${ greeting.breed }.
</body>
</html>