<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Dog Greeting</title>

	<link rel="stylesheet" href="/CapstoneDemo/bootstrap.css">

</head>
<body style="background:linear-gradient(#E6E6E6,#FFFFFF)">
	<div class="container" style="padding:20px">
		<div class="row">
		<div class="col-md-8 col-md-offset-2">
	
		<form:form commandName="formGreeting" class="thumbnail">
			<div class="row text-center"><h2>Dog Things</h2></div>
			<div class="row" style="padding:10px;">
				<div class="col-md-2 text-right">
		   			<form:label class="form-cotnrol" path="name" placeholder="Enter you dog's name">Name</form:label>
		   		</div>
		   		<div class="col-md-8">
		   			<form:input class="form-control" path="name" />
		   		</div>
		   				    
		    </div>
		    
		    <div class="row" style="padding:10px;">
				<div class="col-md-2 text-right">
		   			<form:label class="form-cotnrol" path="age">Age</form:label>
		   		</div>
		   		<div class="col-md-8">
		   			<form:input class="form-control" path="age" />
		   		</div>
		   				    
		    </div>
		    
		    <div class="row" style="padding:10px;">
				<div class="col-md-2 text-right">
		   			<form:label class="form-cotnrol" path="breed">Breed</form:label>
		   		</div>
		   		<div class="col-md-8">
		   			<form:input class="form-control" path="breed" />
		   		</div>
		   				    
		    </div>
		    
		    <div class="row text-right" style="padding:10px;">
	
		   		<div class="col-md-10">
		   			<input class="btn btn-primary" type="submit" value="Submit"/>
		   		</div>
		    
		    </div>
		    
		</form:form>
		
		
		</div>
		</div>
	</div>
	
	<script src="/CapstoneDemo/bootstrap.js"></script>

</body>
</html>