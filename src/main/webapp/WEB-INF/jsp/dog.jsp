<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Dog</title>
	<link rel="stylesheet" href="/CapstoneDemo/bootstrap.css">
</head>
<body>
	<div class="container">
		<button class="btn btn-primary">${dogGreeting}</button>
	</div>
	<script src="/CapstoneDemo/bootstrap.js"></script>
</body>
</html>