package com.metlife.dpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metlife.dpa.model.DogGreeting;
import com.metlife.dpa.model.Greeting;

@Controller
public class DogController {
	@RequestMapping(value="/dog")
	public String sayDogThings(Model model){
		model.addAttribute("dogGreeting", "Bark Bark!");
		return "dog";
	}
	
    @RequestMapping(value="/dogGreeting", method=RequestMethod.GET)
    public String greetingForm(Model model) {
        model.addAttribute("formGreeting", new DogGreeting());
        return "dogGreeting";
    }

    @RequestMapping(value="/dogGreeting", method=RequestMethod.POST)
    public String greetingSubmit(@ModelAttribute DogGreeting dogGreeting, Model model) {
    	System.out.println(dogGreeting.getName());
    	System.out.println(dogGreeting.getAge());
    	System.out.println(dogGreeting.getBreed());
        model.addAttribute("greeting", dogGreeting);
        return "dogResult";
    }
}
